<?php

namespace api\controllers;

use common\models\User;
use yii\filters\AccessControl;
use yii\filters\auth\HttpBasicAuth;
use yii\rest\ActiveController;

/**
 * Class UserController
 * @package api\controllers
 */
class UserController extends ActiveController
{
    public $modelClass = User::class;

    /**
     * @inheritdoc
     */
    public function actions()
    {
        // Закрываем доступы к некоторым экшенам
        $actions = parent::actions();
        unset($actions['create'], $actions['delete']);
        return $actions;
    }

    /**
     * {@inheritdoc}
     */
    public function behaviors()
    {
        $behaviors = parent::behaviors();

        $behaviors['authenticator']['class'] = HttpBasicAuth::class;
        $behaviors['authenticator']['auth'] = function ($username, $password) {
            $user = \api\models\User::findByUsername($username);
            return ($user && $user->validatePassword($password)) ? $user : null;
        };

        $behaviors['access'] = [
            'class' => AccessControl::class,
            'rules' => [
                [
                    'allow' => true,
                    'roles' => ['@'],
                ]
            ],
        ];

        return $behaviors;
    }
}
