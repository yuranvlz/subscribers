<?php
namespace common\models;

use Yii;
use yii\base\NotSupportedException;
use yii\behaviors\TimestampBehavior;
use yii\db\ActiveRecord;
use yii\web\IdentityInterface;

/**
 * User model
 *
 * @property integer $id
 * @property string $username
 * @property string $first_name
 * @property string $last_name
 * @property string $middle_name
 * @property string $password_hash
 * @property string $password_reset_token
 * @property string $email
 * @property string $auth_key
 * @property integer $status
 * @property integer $created_at
 * @property integer $updated_at
 * @property string $password write-only password
 *
 * @property string $fullName
 * @property string $subscriptionFinishDate
 *
 * @property UserSubscription $subscription
 */
class User extends ActiveRecord implements IdentityInterface
{
    const STATUS_DELETED = 0;
    const STATUS_ACTIVE = 10;

    public $password;
    public $subscriptionFinishDate;
    public $fullName;

    public function afterFind()
    {
        $this->fullName = $this->getFullName();
        $this->subscriptionFinishDate = $this->subscription ? Yii::$app->formatter->asDate($this->subscription->finish_date) : null;
    }

    /**
     * @return string
     */
    public function getFullName()
    {
        return ($this->last_name ? ($this->last_name . ' ') : '') . ($this->first_name ? ($this->first_name . ' ') : '') . ($this->middle_name ?: '');
    }

    /**
     * {@inheritdoc}
     */
    public static function tableName()
    {
        return '{{%user}}';
    }

    /**
     * {@inheritdoc}
     */
    public function behaviors()
    {
        return [
            TimestampBehavior::className(),
        ];
    }

    /**
     * {@inheritdoc}
     */
    public function rules()
    {
        return [
            [['id', 'status'], 'integer'],

            ['username', 'trim'],
            ['username', 'required'],
            ['username', 'unique'],
            ['username', 'string', 'min' => 2, 'max' => 255],
            ['username', 'match', 'pattern' => '/^[a-z][a-z0-9]*$/i', 'message' => 'Имя пользователя должно начинаться с буквы и содержать только латинские символы и цифры'],

            ['email', 'trim'],
            ['email', 'required'],
            ['email', 'email'],
            ['email', 'string', 'max' => 255],
            ['email', 'unique'],

            ['password', 'required', 'on' => 'create'],
            ['password', 'string', 'min' => 6],

            [['auth_key', 'password_hash', 'password_reset_token'], 'safe'],
            [['username', 'first_name', 'last_name', 'middle_name', 'fullName', 'subscriptionFinishDate'], 'string'],
            ['status', 'default', 'value' => self::STATUS_ACTIVE],
            ['status', 'in', 'range' => [self::STATUS_ACTIVE, self::STATUS_DELETED]],
        ];
    }

    public function attributeLabels()
    {
        return [
            'username' => 'Логин',
            'first_name' => 'Имя',
            'last_name' => 'Фамилия',
            'middle_name' => 'Отчество',
            'fullName' => 'ФИО',
            'email' => 'Email',
            'password' => 'Пароль',
            'subscriptionFinishDate' => 'Дата окончания подписки',
        ];
    }

    /**
     * {@inheritdoc}
     */
    public static function findIdentity($id)
    {
        return static::findOne(['id' => $id, 'status' => self::STATUS_ACTIVE]);
    }

    /**
     * {@inheritdoc}
     */
    public static function findIdentityByAccessToken($token, $type = null)
    {
        throw new NotSupportedException('"findIdentityByAccessToken" is not implemented.');
    }

    /**
     * Finds user by username
     *
     * @param string $username
     * @return static|null
     */
    public static function findByUsername($username)
    {
        return static::findOne(['username' => $username, 'status' => self::STATUS_ACTIVE]);
    }

    /**
     * Finds user by password reset token
     *
     * @param string $token password reset token
     * @return static|null
     */
    public static function findByPasswordResetToken($token)
    {
        if (!static::isPasswordResetTokenValid($token)) {
            return null;
        }

        return static::findOne([
            'password_reset_token' => $token,
            'status' => self::STATUS_ACTIVE,
        ]);
    }

    /**
     * Finds out if password reset token is valid
     *
     * @param string $token password reset token
     * @return bool
     */
    public static function isPasswordResetTokenValid($token)
    {
        if (empty($token)) {
            return false;
        }

        $timestamp = (int) substr($token, strrpos($token, '_') + 1);
        $expire = Yii::$app->params['user.passwordResetTokenExpire'];
        return $timestamp + $expire >= time();
    }

    /**
     * {@inheritdoc}
     */
    public function getId()
    {
        return $this->getPrimaryKey();
    }

    /**
     * {@inheritdoc}
     */
    public function getAuthKey()
    {
        return $this->auth_key;
    }

    /**
     * {@inheritdoc}
     */
    public function validateAuthKey($authKey)
    {
        return $this->getAuthKey() === $authKey;
    }

    /**
     * Validates password
     *
     * @param string $password password to validate
     * @return bool if password provided is valid for current user
     */
    public function validatePassword($password)
    {
        return Yii::$app->security->validatePassword($password, $this->password_hash);
    }

    /**
     * Generates password hash from password and sets it to the model
     *
     * @param string $password
     */
    public function setPassword($password)
    {
        $this->password_hash = Yii::$app->security->generatePasswordHash($password);
    }

    /**
     * Generates "remember me" authentication key
     */
    public function generateAuthKey()
    {
        $this->auth_key = Yii::$app->security->generateRandomString();
    }

    /**
     * Generates new password reset token
     */
    public function generatePasswordResetToken()
    {
        $this->password_reset_token = Yii::$app->security->generateRandomString() . '_' . time();
    }

    /**
     * Removes password reset token
     */
    public function removePasswordResetToken()
    {
        $this->password_reset_token = null;
    }

    /**
     * @return \yii\db\ActiveQuery
     */
    public function getSubscription(){
        return $this->hasOne(UserSubscription::class, ['user_id' => 'id']);
    }

    public function beforeSave($insert)
    {
        if ($this->password) {
            $this->setPassword($this->password);
            if ($insert) {
                $this->generateAuthKey();
            }
        }

        return parent::beforeSave($insert);
    }

    /**
     * @param bool $insert
     * @param array $changedAttributes
     * @throws \Throwable
     * @throws \yii\db\StaleObjectException
     */
    public function afterSave($insert, $changedAttributes)
    {
        parent::afterSave($insert, $changedAttributes);
        $this->saveSubscription();
    }

    /**
     * @throws \Throwable
     * @throws \yii\db\StaleObjectException
     */
    private function saveSubscription()
    {
        $userSubscription = $this->subscription;
        $finishDate = $this->subscriptionFinishDate ? strtotime($this->subscriptionFinishDate . ' 23:59:59') : null;

        if (
            $userSubscription &&
            !$finishDate
        ) {
            $userSubscription->delete();
        } elseif (
            $userSubscription &&
            $finishDate &&
            $finishDate !== $userSubscription->finish_date
        ){
            $userSubscription->updateAttributes(['finish_date' => $finishDate]);
        } elseif (
            !$userSubscription &&
            $finishDate
        ) {
            $userSubscription = new UserSubscription(['finish_date' => $finishDate]);
            $userSubscription->link('user', $this);
        }
    }

    public function fields()
    {
        return [
            'id' => 'id',
            'username' => 'username',
            'fullName' => function (User $model) {
                return $model->getFullName();
            },
            'subscriptionFinishDate' => 'subscriptionFinishDate',
        ];
    }
}
