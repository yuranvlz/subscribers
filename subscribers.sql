-- phpMyAdmin SQL Dump
-- version 4.7.7
-- https://www.phpmyadmin.net/
--
-- Хост: 127.0.0.1:3306
-- Время создания: Дек 25 2018 г., 10:16
-- Версия сервера: 5.6.38-log
-- Версия PHP: 7.0.26

SET SQL_MODE = "NO_AUTO_VALUE_ON_ZERO";
SET AUTOCOMMIT = 0;
START TRANSACTION;
SET time_zone = "+00:00";


/*!40101 SET @OLD_CHARACTER_SET_CLIENT=@@CHARACTER_SET_CLIENT */;
/*!40101 SET @OLD_CHARACTER_SET_RESULTS=@@CHARACTER_SET_RESULTS */;
/*!40101 SET @OLD_COLLATION_CONNECTION=@@COLLATION_CONNECTION */;
/*!40101 SET NAMES utf8mb4 */;

--
-- База данных: `subscribers`
--

-- --------------------------------------------------------

--
-- Структура таблицы `migration`
--

CREATE TABLE `migration` (
  `version` varchar(180) NOT NULL,
  `apply_time` int(11) DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8;

--
-- Дамп данных таблицы `migration`
--

INSERT INTO `migration` (`version`, `apply_time`) VALUES
('m000000_000000_base', 1545631939),
('m130524_201442_init', 1545634863),
('m181224_062932_create_table_user_subscription', 1545717748);

-- --------------------------------------------------------

--
-- Структура таблицы `user`
--

CREATE TABLE `user` (
  `id` int(11) NOT NULL,
  `username` varchar(255) COLLATE utf8_unicode_ci NOT NULL,
  `first_name` varchar(255) COLLATE utf8_unicode_ci NOT NULL,
  `last_name` varchar(255) COLLATE utf8_unicode_ci NOT NULL,
  `middle_name` varchar(255) COLLATE utf8_unicode_ci NOT NULL,
  `auth_key` varchar(32) COLLATE utf8_unicode_ci NOT NULL,
  `password_hash` varchar(255) COLLATE utf8_unicode_ci NOT NULL,
  `password_reset_token` varchar(255) COLLATE utf8_unicode_ci DEFAULT NULL,
  `email` varchar(255) COLLATE utf8_unicode_ci NOT NULL,
  `status` smallint(6) NOT NULL DEFAULT '10',
  `created_at` int(11) NOT NULL,
  `updated_at` int(11) NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8 COLLATE=utf8_unicode_ci;

--
-- Дамп данных таблицы `user`
--

INSERT INTO `user` (`id`, `username`, `first_name`, `last_name`, `middle_name`, `auth_key`, `password_hash`, `password_reset_token`, `email`, `status`, `created_at`, `updated_at`) VALUES
(1, 'admin', 'Иван', 'Иванов', 'Иванович', 'i3at6wIoShyCtKuZvc3D0ov0Iv55HHge', '$2y$13$C5YPcoScMzLDb99v2gdMae0UE/Jto93UNxFYF.hZ/g8xx8RfSDESm', NULL, 'admin@gmail.com', 10, 1545636611, 1545652695),
(2, 'user1', 'Петр', 'Петров', 'Петрович', 'iBjhkLQZQPFRzll5giIgt3TxRRXvVOoU', '$2y$13$WN3pF7lUYdWJSiJBmM1INuoI5DqQQA1nEYUPu5msbBtA4wRT4br6q', NULL, 'user1@gmail.com', 10, 1545644986, 1545653684),
(6, 'api', '', '', '', 'M7wzxABCVv_UjNcNum4hjvgvDAxP7OsW', '$2y$13$2CKHar7JGiRzXcq6O0re7e9bqIvZs/o83yOYz5svu5S6Gf5Q4CDlW', NULL, 'api@mail.ru', 10, 1545715769, 1545715769),
(7, 'user3', '', '', '', 'KLhYymeebdZw4oOaQot1j2M9OnQiHw10', '$2y$13$KeejegYIynj9omWVnISmCeu5jPAPYJpAR4wjsqreteZUUCm3Xz0zK', NULL, 'user3@gmail.com', 10, 1545720492, 1545720492),
(8, 'user44', 'иван 6', 'Иванов', 'Иванович', 'DmmNsTYqSRH0sR9oGevlqJZX9WUVAKYe', '$2y$13$/vHiQ0ZaCOa6HIUpCe097uL2g.4lXiARz3y0A6JRzB35HeDoFT6eC', NULL, 'user4@mail.ru', 10, 1545720591, 1545721439);

-- --------------------------------------------------------

--
-- Структура таблицы `user_subscription`
--

CREATE TABLE `user_subscription` (
  `id` int(11) NOT NULL,
  `user_id` int(11) NOT NULL,
  `created_at` int(11) NOT NULL,
  `updated_at` int(11) NOT NULL,
  `finish_date` int(11) NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8 COLLATE=utf8_unicode_ci;

--
-- Дамп данных таблицы `user_subscription`
--

INSERT INTO `user_subscription` (`id`, `user_id`, `created_at`, `updated_at`, `finish_date`) VALUES
(1, 1, 1545717850, 1545717850, 1545857999),
(2, 2, 1545718321, 1545718321, 1545944399),
(4, 8, 1545720598, 1545720598, 1545598799);

--
-- Индексы сохранённых таблиц
--

--
-- Индексы таблицы `migration`
--
ALTER TABLE `migration`
  ADD PRIMARY KEY (`version`);

--
-- Индексы таблицы `user`
--
ALTER TABLE `user`
  ADD PRIMARY KEY (`id`),
  ADD UNIQUE KEY `username` (`username`),
  ADD UNIQUE KEY `email` (`email`),
  ADD UNIQUE KEY `password_reset_token` (`password_reset_token`);

--
-- Индексы таблицы `user_subscription`
--
ALTER TABLE `user_subscription`
  ADD PRIMARY KEY (`id`),
  ADD KEY `idx-user_subscription-user_id` (`user_id`);

--
-- AUTO_INCREMENT для сохранённых таблиц
--

--
-- AUTO_INCREMENT для таблицы `user`
--
ALTER TABLE `user`
  MODIFY `id` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=9;

--
-- AUTO_INCREMENT для таблицы `user_subscription`
--
ALTER TABLE `user_subscription`
  MODIFY `id` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=5;

--
-- Ограничения внешнего ключа сохраненных таблиц
--

--
-- Ограничения внешнего ключа таблицы `user_subscription`
--
ALTER TABLE `user_subscription`
  ADD CONSTRAINT `fk-user_subscription-user_id` FOREIGN KEY (`user_id`) REFERENCES `user` (`id`) ON DELETE CASCADE;
COMMIT;

/*!40101 SET CHARACTER_SET_CLIENT=@OLD_CHARACTER_SET_CLIENT */;
/*!40101 SET CHARACTER_SET_RESULTS=@OLD_CHARACTER_SET_RESULTS */;
/*!40101 SET COLLATION_CONNECTION=@OLD_COLLATION_CONNECTION */;
