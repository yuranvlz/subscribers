<?php

namespace backend\models;

use common\models\User;
use common\models\UserSubscription;
use yii\data\ActiveDataProvider;

/**
 * UserSearch represents the model behind the search form of `common\models\User`.
 */
class UserSearch extends User
{
    public function rules()
    {
        return [
            [['id'], 'integer'],
            [['username', 'fullName', 'email', 'subscriptionFinishDate'], 'string'],
        ];
    }

    /**
     * Creates data provider instance with search query applied
     *
     * @param array $params
     *
     * @return ActiveDataProvider
     */
    public function search($params)
    {
        $query = User::find();

        // add conditions that should always apply here

        $dataProvider = new ActiveDataProvider([
            'query' => $query,
            'sort' => [
                'attributes' => [
                    'id',
                    'username',
                    'fullName' => [
                        'asc' => [User::tableName().'.last_name' => SORT_ASC, User::tableName().'.first_name' => SORT_ASC],
                        'desc' => [User::tableName().'.last_name' => SORT_DESC, User::tableName().'.first_name' => SORT_DESC],
                    ],
                    'subscriptionFinishDate' => [
                        'asc' => [UserSubscription::tableName().'.finish_date' => SORT_ASC],
                        'desc' => [UserSubscription::tableName().'.finish_date' => SORT_DESC],
                    ],
                    'email'
                ]
            ],
        ]);

        $this->load($params);

        if (!$this->validate()) {
            // uncomment the following line if you do not want to return any records when validation fails
            // $query->where('0=1');
            return $dataProvider;
        }

        $query->joinWith('subscription');

        // Преобразуем дату окончания подписки
        $subscriptionFinishDate = $this->subscriptionFinishDate ? strtotime($this->subscriptionFinishDate . ' 23:59:59') : null;

        // grid filtering conditions
        $query
            ->andFilterWhere([
                User::tableName() . '.id' => $this->id,
                'status' => $this->status,
                UserSubscription::tableName() . '.finish_date' => $subscriptionFinishDate,
            ])
            ->andFilterWhere(['like', 'lower(username)', mb_strtolower(trim($this->username))])
            ->andFilterWhere(['like', 'lower(email)', mb_strtolower(trim($this->email))]);

        // Разбиваем полное имя на слова и ищем каждое слово в трех колонках (имя, фамилия, отчество)
        list($lastName, $firstName, $midName) = explode(' ', mb_strtolower($this->fullName));
        $query->andFilterWhere(['lower(last_name)' => trim($lastName)]);
        $query->andFilterWhere(['lower(first_name)' => trim($firstName)]);
        $query->andFilterWhere(['lower(middle_name)' => trim($midName)]);

        return $dataProvider;
    }
}
