<?php

use yii\helpers\Html;
use yii\grid\GridView;

/* @var $this yii\web\View */
/* @var $searchModel backend\models\UserSearch */
/* @var $dataProvider yii\data\ActiveDataProvider */

$this->title = 'Пользователи';
$this->params['breadcrumbs'][] = $this->title;
?>
<div class="user-index">

    <h1><?= Html::encode($this->title) ?></h1>
    <?php // echo $this->render('_search', ['model' => $searchModel]); ?>

    <p>
        <?= Html::a('Создать нового пользователя', ['create'], ['class' => 'btn btn-success']) ?>
    </p>

    <?= GridView::widget([
        'dataProvider' => $dataProvider,
        'filterModel' => $searchModel,
        'columns' => [
//            ['class' => 'yii\grid\SerialColumn'],

            'id',
            'username',
            'fullName',
            'email:email',
            [
                'attribute' => 'subscriptionFinishDate',
                'filter' => \omnilight\widgets\DatePicker::widget([
                    'model' => $searchModel,
                    'attribute' => 'subscriptionFinishDate',
                    'language' => 'ru',
                    'options' => [
                        'class' => 'form-control',
                    ],
                    'clientOptions' => [
                        'clearBtn' => true,
                    ],
                ]),
                'format' => 'date',
            ],
            //'status',
            //'created_at',
            //'updated_at',

            [
                'class' => '\yii\grid\ActionColumn',
                'template' => '{update} {delete}',
            ],
        ],
    ]); ?>
</div>
